<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite6f5b5116b94dbecc5d5d62ab7af43b9
{
    public static $prefixLengthsPsr4 = array (
        'm' => 
        array (
            'myfw\\' => 5,
        ),
        'a' => 
        array (
            'app\\' => 4,
        ),
        'R' => 
        array (
            'RedBeanPHP\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'myfw\\' => 
        array (
            0 => __DIR__ . '/..' . '/myfw/core',
        ),
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
        'RedBeanPHP\\' => 
        array (
            0 => __DIR__ . '/..' . '/gabordemooij/redbean/RedBeanPHP',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite6f5b5116b94dbecc5d5d62ab7af43b9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite6f5b5116b94dbecc5d5d62ab7af43b9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
