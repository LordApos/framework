<?php

namespace app\controllers;

use RedBeanPHP\R;

class MainController extends AppController {

//    public $layout = 'test';

    public function indexAction(){
        $logins = R::findAll('users');
        $lordapos = \R::findOne('users', 'login = ?', ['LordApos']);
        $this->setMeta('Главная страница', 'Описание...', 'Ключевики...');
        $this->set(compact('logins', 'lordapos'));
    }

}
